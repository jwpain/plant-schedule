import sys, os
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
# build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "PlantScheduler",
		version = '1',
   		options = {'build_exe': {'includes':['sqlite3'],'include_files': [os.path.join(sys.base_prefix, 'DLLs', 'sqlite3.dll')]}},
        executables = [Executable("plant.py", base=base, icon='tree.ico')])