import xlsxwriter
import string
import datetime, time


class Excel():

	def __init__(self, path):
		st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H%M%S')
		self.workbook = xlsxwriter.Workbook(str(path) + '/' + st + '.xlsx')
		self.worksheet = self.workbook.add_worksheet()

	def create(self,data):
		
		num2alpha = dict(zip(range(1, 27), string.ascii_uppercase))

		for i, row in enumerate(data):
			for j, col in enumerate(row):
				self.worksheet.write(num2alpha[j+1] + str(i+1),col)

		self.workbook.close()