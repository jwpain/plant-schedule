#!/usr/bin/python3

from PyQt5.QtWidgets import QApplication 
from PyQt5.QtGui import QIcon
from PyQt5.QtWinExtras import QWinTaskbarButton
from PyQt5.QtCore import Qt

from gui import Table, Example
import sys
import ctypes
 
if __name__ == '__main__':
	
	# Allow application icon to be set
	myappid = 'plant-schedule'
	ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

	# Start Application
	app = QApplication(sys.argv)
	app.setWindowIcon(QIcon('tree.ico'))
	ex = Example()
	sys.exit(app.exec_())