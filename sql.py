import sqlite3

class Database():

	def __init__(self):
		self.db_path = 'plants.db'
		self.column_names = self.get_column_names()

	def get_data(self):
		conn = sqlite3.connect(self.db_path)
		c = conn.cursor()

		data = []
		# Get data
		for row in c.execute('SELECT * FROM plants_database'):
			data.append(list(row))

		# Save (commit) the changes
		conn.commit()

		# We can also close the connection if we are done with it.
		# Just be sure any changes have been committed or they will be lost.
		conn.close()

		return data

	def search_data(self,s):
		conn = sqlite3.connect(self.db_path)
		c = conn.cursor()

		data = []

		statement = "SELECT Botanical_Name, Abbreviation, Common_Name, Size, Spacing, Height, Width, Type FROM plants_database WHERE Botanical_Name LIKE '%" + str(s) + "%'" + \
		"OR Abbreviation LIKE '%" + str(s) + "%'" + \
		"OR Common_Name LIKE '%" + str(s) + "%'" + \
		"OR Size LIKE '%" + str(s) + "%'" + \
		"OR Type LIKE '%" + str(s) + "%'"

		# Get data
		for row in c.execute(statement):
			data.append(list(row))

		# Save (commit) the changes
		conn.commit()

		# We can also close the connection if we are done with it.
		# Just be sure any changes have been committed or they will be lost.
		conn.close()
		return data

	def get_column_names(self):
		conn = sqlite3.connect(self.db_path)
		c = conn.cursor()

		data = []
		# Get data
		for row in c.execute("PRAGMA table_info(plants_database)"):
			data.append(row[1])

		# Save (commit) the changes
		conn.commit()

		# We can also close the connection if we are done with it.
		# Just be sure any changes have been committed or they will be lost.
		conn.close()
		return data