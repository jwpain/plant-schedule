from PyQt5.QtWidgets import QApplication, QTableWidget, QTableWidgetItem, QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QAbstractItemView, QLineEdit, QFileDialog, QLabel, QDialog
from PyQt5.QtWinExtras import QWinTaskbarButton
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt

from sql import Database
from xlsx import Excel
import urllib.request


class Table():
	
	def __init__(self):
		self.table = QTableWidget()
		tableItem = QTableWidgetItem()
		self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
		self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
		self.empty = True

	def get(self):
		return self.table

	def set_data(self, data):

		self.table.setRowCount(len(data))
		try:
			self.table.setColumnCount(len(data[0]))
		except:
			pass

		for i,row in enumerate(data):
			for j,col in enumerate(row):
				self.table.setItem(i,j,QTableWidgetItem(str(col)))

	def add_row(self,row):

		duplicate = False
		print(self.table.columnCount())
		if self.empty != True:
			for j in range(0,self.table.rowCount()):
				if self.table.item(j,0).text() == row[0]:
					duplicate = True
					break

		if (duplicate == False) or (self.empty == True):

			self.empty = False
			row_count = self.table.rowCount()
			self.table.setRowCount(row_count + 1)

			for i, item in enumerate(row):
				self.table.setItem(row_count,i,QTableWidgetItem(str(item)))

	def get_row(self,index):

		data = []
		for rows in index:
			for i in range(0,self.table.columnCount()):
				data.append(self.table.item(rows,i).text())

		return data

	def get_data(self):

		data = []
		for i in range(0,self.table.rowCount()):
			row = []
			for j in range(0,self.table.columnCount()):
				row.append(self.table.item(i,j).text())
			data.append(row)
				
		return data

	def set_column_count(self,data):
		self.table.setColumnCount(len(data[0]))

	def delete_row(self,index):

		for i in index:
			self.table.removeRow(i)

		if self.table.rowCount() == 0:
			self.empty = True


class Button():

	def __init__(self,label):
		self.item = QPushButton(label)
		
class SearchBox():

	def __init__(self,label):
		self.item = QLineEdit()

class Example(QWidget):
	
	def __init__(self):
		super().__init__()
		
		self.initUI()

	def initUI(self):

		self.setGeometry(200, 200, 1024, 400)
		self.setWindowTitle('Plant Schedule')    
		
		d = Database()
		data = d.get_data()
		headers = d.get_column_names()
		self.t_1 = Table()
		self.t_1.set_data(data)
		# self.t_1.table.itemSelectionChanged.connect(self.load_image)
		self.t_1.table.doubleClicked.connect(self.button_clicked)


		self.t_2 = Table()
		self.t_2.set_column_count(data)
		self.t_2.table.doubleClicked.connect(self.button_delete)

		self.t_1.table.setHorizontalHeaderLabels(headers)
		self.t_2.table.setHorizontalHeaderLabels(headers)

		self.b = Button('Copy')
		self.b.item.clicked.connect(self.button_clicked)
		copyButton = self.b.item

		self.b2 = Button('Save')
		self.b2.item.clicked.connect(self.button_saved)
		saveButton = self.b2.item

		self.b3 = Button('Delete')
		self.b3.item.clicked.connect(self.button_delete)
		deleteButton = self.b3.item

		self.sbox = SearchBox('Search')
		self.sbox.item.textChanged[str].connect(self.search)
		searchBox = self.sbox.item

		self.b4 = Button('Show Image')
		self.b4.item.clicked.connect(self.showdialog)
		winButton = self.b4.item

		hbox = QHBoxLayout()
		vbox = QVBoxLayout()
		# hbox.addStretch(1)
		vbox.addWidget(searchBox)
		vbox.addWidget(copyButton)
		vbox.addWidget(deleteButton)
		vbox.addWidget(saveButton)
		vbox.addWidget(winButton)

		# self.pic = QLabel(self)
		# vbox.addWidget(self.pic)
		vbox.addStretch(1)

		hbox.addLayout(vbox,1)
		hbox.addWidget(self.t_1.get(),4)
		hbox.addWidget(self.t_2.get(),4)

		self.setLayout(hbox)

		self.show()

	def button_clicked(self):
		sender = self.sender()

		index = [idx.row() for idx in self.t_1.table.selectionModel().selectedRows()]
		row = self.t_1.get_row(index)
		self.t_2.add_row(row)

	def button_saved(self):

		def pick_directory():
			dialog = QFileDialog()
			folder_path = dialog.getExistingDirectory(None, "Select Save Directory")
			return folder_path

		directory = pick_directory()

		if directory is not '':

			d = Database()
			data = [d.get_column_names()] + self.t_2.get_data()
			new_excel = Excel(directory)
			new_excel.create(data)

	def button_delete(self):
		indexes = [idx.row() for idx in self.t_2.table.selectionModel().selectedRows()]
		self.t_2.delete_row(indexes)

	def search(self,text):
		d = Database()
		new_data = d.search_data(text)
		if new_data is not None:
			self.t_1.set_data(new_data)

	def load_image(self):
		
		index = [idx.row() for idx in self.t_1.table.selectionModel().selectedRows()]
		row = self.t_1.get_row(index)
		print(row[-1])
		from urllib import request
		x = request.urlretrieve(row[-1], "local-filename.jpg")


	def showdialog(self):
		try:
			d = QDialog()
			d.setFixedSize(500,500)
			# b1 = QPushButton("ok",d)
			# b1.move(50,50)
			d.setWindowTitle("Dialog")
			d.pic = QLabel(d)
			
			index = [idx.row() for idx in self.t_1.table.selectionModel().selectedRows()]
			row = self.t_1.get_row(index)
			print(row[-1])
			from urllib import request
			x = request.urlretrieve(row[-1], "local-filename.jpg")

			y = QPixmap("local-filename.jpg")
			z = y.scaled(500,500, Qt.KeepAspectRatio)
			d.pic.setPixmap(z)
			d.pic.show()
			# self.pi
			d.exec_()
		except:
			pass
	